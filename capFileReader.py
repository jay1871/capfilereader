# basic CAP file reader class based - requires numpy to be available.
# https://www.programcreek.com/python/example/8254/numpy.fromfile

import numpy as np

class capFileReader:

    def __init__(self):
        # this was a union in c but just using it as a char
        self.E328S_GUID = np.dtype(
            [('c', np.dtype('>u1'), 16)]
        )

        # follows the C header file
        self.BigSignalCaptureHdr = np.dtype(
            [('version', np.dtype('>S8')),
             ('timeStamp', np.dtype('>f8')),
             # yes read it as a float because it looks like it is in s!! in floating point from 1970
             ('span', np.dtype('>f8')),
             ('centerFreq', np.dtype('>f8')),
             ('scalar', np.dtype('>f8')),
             ('deltaT', np.dtype('>f8')),
             ('freqOffset', np.dtype('>f8')),
             ('latitude', np.dtype('>f8')),
             ('longitude', np.dtype('>f8')),
             ('heading', np.dtype('>f4')),
             ('altitude', np.dtype('>f4')),
             ('signalGuid', self.E328S_GUID),
             ('energyGuid', self.E328S_GUID),
             ('taskingGuid', self.E328S_GUID),
             ('antennaGuid', self.E328S_GUID),
             ('dblPad', np.dtype('>f8'), 14),
             ('dataType', np.dtype('>u4')),
             ('adcOverload', np.dtype('>u4')),
             ('complex', np.dtype('>u4')),
             ('mirror', np.dtype('>u4')),
             ('numBlocks', np.dtype('>u4')),
             ('blocksize', np.dtype('>u4')),
             ('channel', np.dtype('>u4')),
             ('syncStatus', np.dtype('>u4')),
             ('corrected', np.dtype('>u4')),
             ('locationValid', np.dtype('>u4')),
             ('fromPlayback', np.dtype('>u4')),
             ('timeStampSeconds', np.dtype('>u4')),
             ('timeStampNSeconds', np.dtype('>u4')),
             ('pad', np.dtype('>u4'), 18),
             ('userHeaderType', np.dtype('>u4')),
             ('userHeader', np.dtype('>u1'), 128)
             ]
        )

        # let's setup some defaults that are used
        self.validHdr = False     # used to determine if a header has been read.
        self.windowSize = 4096   # this is number of real or complex samples to look at
        self.overlapPercent = 0  # this is percentage in int from windowing - only 0, 25, 50, 75 are valid

    def openCapFile(self,filename):  # get cap file and reads hdr info and closes it
        assert isinstance(filename, object)
        self.filename = filename
        try:
            file = open(filename, "r")
            # opened file, get header info now.
            self.readHdr()
            # close it since it is not useful.
            file.close()
        except IOError:
            print("Error Opening file " + filename)
            return 0

    def readHdr(self):
        self.HdrStruct = np.fromfile(self.filename, dtype=self.BigSignalCaptureHdr)
        # now let's parse it into a structure.
        self.version = self.HdrStruct['version'][0].decode('UTF-8')
        self.timestamp = self.HdrStruct['timeStamp'][0].astype('datetime64[s]')
        self.span = self.HdrStruct['span'][0]
        self.centerFreq = self.HdrStruct['centerFreq'][0]
        self.scalar = self.HdrStruct['scalar'][0]
        self.deltaT = self.HdrStruct['deltaT'][0]
        self.freqOffset = self.HdrStruct['freqOffset'][0]
        self.latitude = self.HdrStruct['latitude'][0]
        self.longitude = self.HdrStruct['longitude'][0]
        self.heading = self.HdrStruct['heading'][0]
        self.altitude = self.HdrStruct['altitude'][0]
        self.signalGuid = self.HdrStruct['signalGuid'][0]
        self.energyGuid = self.HdrStruct['energyGuid'][0]
        self.taskingGuid = self.HdrStruct['taskingGuid'][0]
        self.antennaGuid = self.HdrStruct['antennaGuid'][0]
        self.datatype = self.HdrStruct['dataType'][0]
        self.adcOverload = self.HdrStruct['adcOverload'][0]
        self.complex = self.HdrStruct['complex'][0]
        self.mirror = self.HdrStruct['mirror'][0]
        self.numBlocks = self.HdrStruct['numBlocks'][0]
        self.blocksize = self.HdrStruct['blocksize'][0]
        self.channel = self.HdrStruct['channel'][0]
        self.syncStatus = self.HdrStruct['syncStatus'][0]
        self.corrected = self.HdrStruct['corrected'][0]
        self.locationValid = self.HdrStruct['locationValid'][0]
        self.fromPlayback = self.HdrStruct['fromPlayback'][0]
        self.timeStampSeconds = self.HdrStruct['timeStampSeconds'][0]
        self.timeStampNSeconds = self.HdrStruct['timeStampNSeconds'][0]
        self.userHeaderType = self.HdrStruct['userHeaderType'][0]

        self.validHdr = True # set it to true now since a header has been read.
        self.computeTotalSamples()
        self.computeMaxIndices()

    def setOverlap(self, percent):   # sets the overlap percentage
        validpercent = [0, 25, 50, 75]
        if (validpercent.count(percent)) == 1:
            self.overlapPercent = percent
            self.computeMaxIndices()
        else:
            print("% overlap must be 0/25/50/75 + you had "+ percent)
    #
    def getOverlap(self):
        return self.overlapPercent

    def computeTotalSamples(self):
        if self.validHdr == True:
            self.totalSamples = self.numBlocks * self.blocksize / (self.complex + 1)
        self.totalSamples = self.totalSamples.astype(np.int64)

    def getTotalSamples(self):
        return self.totalSamples

    # this must be called when overlap percent changes!!!
    def computeMaxIndices(self):
        self.maxIndices = (self.totalSamples/self.windowSize) * (100 / (100 - self.overlapPercent))
        self.maxIndices = self.maxIndices.astype(np.int64)

    def getMaxIndices(self):
        return(self.maxIndices)

    def setWindowSize(self, size):
        if (type(size) == int):
            self.windowSize = size
            self.computeMaxIndices()
        else:
            print("Window Size is not an integer - " + size)

    def getWindowSize(self):
        return(self.windowSize)

    def printHdr(self):  # prints header info out
        if (self.validHdr == True):
            print("Version: " + self.version)
            print("Timestamp: " + self.timestamp.astype(str))
            print("Center Freq (MHz): " + (self.centerFreq / 1.0e6).astype(str))
            print("Span (MHz): " + (self.span / 1.0e6).astype(str))
            print("DeltaT (ns): " + (self.deltaT * 1.0e9).astype(str))
            print("Fsample (MHz): " + ((1.0/self.deltaT)/1.0e6).astype(str))
            print("Freq. Offset (MHz): " + (self.freqOffset / 1.0e6).astype(str))
            print("Latitude: " + self.latitude.astype(str))
            print("Longitude: " + self.longitude.astype(str))
            print("Alt: " + self.altitude.astype(str))
            print("Datatype: " + self.datatype.astype(str))
            print("Complex: " + self.complex.astype(str))
            print("Mirrored: " + self.mirror.astype(str))
            print("numBlocks: " + self.numBlocks.astype(str))
            print("blockSize: " + self.blocksize.astype(str))
        else:
            printf("No valid header read from file")

    def getSamples(self, index):  # returns windowsize of samples based on index


        if (type(index) == int):
            elementsperwindow = self.windowSize * (self.complex + 1)

            rawdata = []
            if ((self.complex == 1) and (self.datatype == 0)):  # complex 16 bits
                rawdata = np.memmap(self.filename, dtype='>u2', mode='r',
                                    offset=self.BigSignalCaptureHdr.itemsize + index * elementsperwindow * np.dtype('u2').itemsize,
                                    shape=(2 * self.windowSize))
            elif ((self.complex == 1) and (self.datatype == 1)):  # complex 32 bits
                rawdata = np.memmap(self.filename, dtype='>u4', mode='r',
                                    offset=self.BigSignalCaptureHdr.itemsize + index * elementsperwindow * np.dtype('u4').itemsize,
                                    shape=(2 * self.windowSize))
            elif ((self.complex == 0) and (self.datatype == 0)):  # real 16 bits
                rawdata = np.memmap(self.filename, dtype='>u2', mode='r',
                                    offset=self.BigSignalCaptureHdr.itemsize + index * elementsperwindow * np.dtype('u2').itemsize,
                                    shape=self.windowSize)
            elif ((self.complex == 0) and (self.datatype == 1)):  # real 32 bits
                rawdata = np.memmap(self.filename, dtype='>u4', mode='r',
                                    offset=self.BigSignalCaptureHdr.itemsize + index * elementsperwindow * np.dtype('u4').itemsize,
                                    shape=self.windowSize)

            # 2s complement back to base 10:  get magnitude - maxvalue if sign bit is there.
            bits = 16 * (self.datatype + 1)
            maxvalue = 2 ** bits

            twocomp2int = lambda t: (t - maxvalue) if ((t >> (bits - 1)) == 1) else t
            vectorized_twoscomp2int = np.vectorize(twocomp2int, otypes=[np.int], cache=False)
            data = vectorized_twoscomp2int(rawdata)

            # if complex, make it complex now - real, complex, etc...
            if (self.complex == 1):
                data = data[0::2] + 1j * data[1::2]
            return(data)
        else:
            return(np.empty([1,1]))

# main function
def main():
    tester = capFileReader()
    tester.openCapFile('d:/drone-data/IQ_18-08-15_21-35-09.992000Z_6Sec_2.42GHz_40MHz.cap')
    tester.printHdr()
    data = tester.getSamples(1)

# execution main
if __name__ == "__main__":
    main()