# test code to generate spectrogram from cap file
import numpy as np
from scipy import signal
from scipy import io
import matplotlib.pyplot as plt
from capFileReader import capFileReader

reader = capFileReader()
reader.openCapFile('d:/drone-data/IQ_18-08-15_21-35-09.992000Z_6Sec_2.42GHz_40MHz.cap')
reader.printHdr()
reader.setWindowSize(4096*16)
windowsize = reader.getWindowSize()

maxindex = reader.getMaxIndices()

# if you want to dump stuff out to matlab - change the index.
# io.savemat('matlabfile.mat', dict(['iq', reader.getSamples(1)]))
Fs = ((1.0/reader.deltaT)/1.0e6)
f, t, Sxx = signal.spectrogram(reader.getSamples(1), Fs)
plt.pcolormesh(t, f, Sxx)
plt.show()
